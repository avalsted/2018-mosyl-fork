// BTL = Basic TAPL language (from the TAPL book)
//       (not to be confused with Bacon Tomato Lettuce)
// This version implemented after "Software Languages" Ralf Lammel. Springer 2018

object BTL {

  sealed trait Expr
  case object True extends Expr
  case object False extends Expr
  case object Zero extends Expr
  case class Succ (inner: Expr) extends Expr // successor
  case class Pred (inner: Expr) extends Expr // predecessor
  case class IsZero (inner: Expr) extends Expr
  case class If (cond: Expr, Then: Expr, Else: Expr) extends Expr

  // I am developing a slightly different evaluator than in the book.
  // Not better or worse, just different.   It follows the same rules, but
  // represents the output not as an Expr, but as an Int.
  //
  // Big Step

  def evaluate (e: Expr): Either[Int,Boolean] = e match {
    case True => Right (true)
    case False => Right (true)
    case Zero => Left (0)
    case (Succ (e)) =>
      evaluate (e) match { case Left (n) => Left (n+1) }
    case (Pred (e)) =>
      evaluate (e) match {
        case Left (0) => Left (0)
        case Left (n) if n > 0 => Left (n-1) }
    case (IsZero (e)) =>
      evaluate (e) match { case Left (n) => Right (n==0) }
    case If (e0, e1, e2) =>
      evaluate (e0) match {
        case Right (true) => evaluate (e1)
        case Right (false) => evaluate (e2)
      }
  }

  // It is worth comparing the following aspects with the book's Haskell
  // implementation:
  //
  // 1. We use Either[Int,Boolean] instead of Expr; using Expr (in the book)
  //    makes all results the same type (no need to use Either); I find
  //    producing Int and Boolean slightly more natural.  The difference is
  //    minor though.
  // 2. In this interpreter, we never return undefined.  It will simply crash on
  //    a wrongly typed expression (so the expression should be first type
  //    checked).  This is also why the compiler produces warnings -- I am
  //    putting up with them to make the code short (fit on a projection).
  //    Normally, one should catch the failure cases explicitly, like in the
  //    book.
  //
  // Observe that the above is basically a solution for Exercise 8.2 in the
  // book.

  // Now for the small-step

  def step (e: Expr): Expr = e match {
    case Succ (e) => step (e) match { case e_ => Succ (e_) }
    case Pred (e) => step (e) match { case e_ => Pred (e_) }
    case Pred (Zero) => Zero
    case Pred (Succ (n)) => n
    case IsZero (e) => step (e) match { case e1 => IsZero (e1) }
    case IsZero (Zero) => True
    case IsZero (Succ(n)) => False // it is important that the first IsZero rule is first and that we have the Pred-Succ rule
    case If (e0, e1, e2) => step (e) match { case e_ => If (e_,e2,e2) }
    case If (True, e1, e2) => e1
    case If (False, e1, e2) => e2
  }

  // transitive closure of the small-step rewrites:

  def steps (f: Expr => Expr) (e: Expr): Expr =
    if (isValue (e))
      e
    else f(e) match {
      case e_ => steps (f) (e_)
    }

  // isValue

  def isNat (e: Expr): Boolean = e match {
    case Zero => true
    case Succ (e) => isNat (e)
    case _ => false
  }

  def isBool (e: Expr): Boolean = e match {
    case True | False => true
    case _ => false
  }

  def isValue (e: Expr): Boolean = isBool (e) || isNat (e)




}
