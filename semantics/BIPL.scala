// BIPL = Basic Imperative Programming Language
// After "Software Languages" Ralf Lammel. Springer 2018

object BIPL {

  sealed trait Stmt
  case object Skip extends Stmt
  case class Assign (id: String, e: Expr) extends Stmt
  case class Seq (s0: Stmt, s1: Stmt) extends Stmt
  case class If (e: Expr, s0: Stmt, s1: Stmt) extends Stmt
  case class While (e: Expr, s: Stmt) extends Stmt

  sealed trait Expr
  case class IntConst (n: Int) extends Expr
  case class Var (id: String) extends Expr
  case class Unary (op: UOp, e: Expr) extends Expr
  case class Binary (op: BOp, e0: Expr, e1: Expr) extends Expr

  sealed trait UOp
  case object Negate extends UOp
  case object Not extends UOp

  sealed trait BOp
  case object Or extends BOp
  case object And extends BOp
  case object Lt extends BOp
  case object LEq extends BOp
  case object Eq extends BOp
  case object GEq extends BOp
  case object Gt extends BOp
  case object Add extends BOp
  case object Sub extends BOp
  case object Mul extends BOp

  type Value = Either[Int,Boolean]
  type Store = Map[String, Value]

  def execute (s: Stmt, m: Store): Store = s match {
    case Skip => m
    case Assign (id,e) => m + (id -> evaluate (e,m))
    case Seq (s1,s2) =>
      val m1 = execute (s1, m)
      execute (s2,m1)
    case If (e, s1, s2) if (evaluate (e,m) == Right (true)) => execute (s1,m)
    case If (e, s1, s2) if (evaluate (e,m) == Right(false)) => execute (s2,m)
    case While (e,s) => execute (If (e, Seq(s, While (e,s)), Skip), m)
  }

  def evaluate (e: Expr, m: Store): Value = e match {
    case IntConst (n) => Left (n)
    case Var (id) => m (id)
    case Unary (Negate, e) =>
      evaluate (e,m) match { case Left (n) => Left (-n) }
    // ...
    case Binary (Add, e0, e1) =>
      val v0 = evaluate (e0, m)
      val v1 = evaluate (e1, m)
      (v0,v1) match { case (Left(n0), Left(n1)) => Left (n0 + n1) }
    // ...
  }

}


