// BIPL = Basic Imperative Programming Language
// After "Software Languages" Ralf Lammel. Springer 2018

object BIPL {

  sealed trait Stmt
  case object Skip extends Stmt
  case class Assign (id: String, e: Expr) extends Stmt
  case class Seq (s0: Stmt, s1: Stmt) extends Stmt
  case class If (e: Expr, s0: Stmt, s1: Stmt) extends Stmt
  case class While (e: Expr, s: Stmt) extends Stmt

  sealed trait Expr
  case class IntConst (n: Int) extends Expr
  case class Var (id: String) extends Expr
  case class Unary (op: UOp, e: Expr) extends Expr
  case class Binary (op: BOp, e0: Expr, e1: Expr) extends Expr

  sealed trait UOp
  case object Negate extends UOp // this is the unary minus
  case object Not extends UOp

  sealed trait BOp
  case object Or extends BOp
  case object And extends BOp
  case object Lt extends BOp
  case object LEq extends BOp
  case object Eq extends BOp
  case object GEq extends BOp
  case object Gt extends BOp
  case object Add extends BOp
  case object Sub extends BOp
  case object Mul extends BOp

}
