The code repository for the book is found at

https://bitbucket.org/itu-square/mdsebook/branch/cmdline_ecore_generator

Please note that this code is work in progress, so not all information in the
repository is up-to-date, and not all the code is properly cleaned and
maintained. I hope you can bear with us, and understand that development of so
much material takes time.

In particular, not all documentation in the tree is correct.  Communicate with
Andrzej (on the forum) when in doubt.  The README file you are reading overrides
anything that repo says to the contrary. But please read the top-level README in
the file.

We will be using the 'cmdline_ecore_generetor' branch -- not the 'master'
branch!!!  To get this branch follow these git commands:

git clone https://andrzej_wasowski@bitbucket.org/itu-square/mdsebook.git
git fetch
git checkout cmdline_ecore_generator

We recommend that you fork the repository on bitbucket and work in your copy.
Then you will be able to archive your own changes.

This code tree uses gradle for build (work in progress) which we highly
recommend over the built-in Eclipse builders.  This allows you to run the build
in a reproducible manner, independent of what version of Eclipse you are using.
In a professional setting it also allows running the build in a CI setup, etc.

1) Ecore meta-modeling

The meta-model project for FSM is mdsebook.fsm (this is in general true for this
tree: the meta-model project for language X is in mdsebook.X/ ).

To view the model open Eclipse (with installed modeling tools) and import the
projects from the git checkout into your workspace.  You can then open and
change the ecore file.

We recommend using Eclipse for editing but not for code generation (as most
online tutorials would tell you).  You create  ecore models and genmodels as
most tutorials refer to you (also the one in the book's appendix).  It is
important not to use nested packages in Ecore models, as this breaks generators.
Instead, change the basePackage property in genmodel to place the generated code
in a desired package.

The Ecore projects in the repo have gradle
build files.  See for example mdsebook.fsm/build.gradle .

To generate code from a given .ecore/.genmodel pair for fsm run:

./gradlew :mdsebook.fsm:compileJava

The command should be run in the top directory of the git repo.  Use gradlew.bat
on Windows instead.  You do not have to install gradle.  All you need is
included in the repo, but an active Internet connection is required.

The generated source code will be places in mdsebook.fsm/src (this location is
configurable in the genmodel file.)

In your own project, to build the model file you will need to adjust lines with
'task.create',  'clean' and 'compileJava' to the name of your language.  The
build setup is quite simple, so be sure to follow the same project layout as
mdsebook.fsm, and the same capitalization of task names.

You also need to add your new project to /settings.gradle.

2) Constraints

The fsm project (and some others) have examples of constraints implemented in
Scala.  These are placed in a dependent project mdsebook.fsm.scala.  Search for
code in the object mdsebook.fsm.scala.Constraints. They use a small library we
implemented, EMFScala, to ease the integration of Scala with EMF.  This library
is also included in the code tree.

The object mdsebook.fsm.scala.Main contains the main function checking the
constraints on some fixed models (analogously  mdsebook.printers.scala.Main for
the printers meta-model). Use the run target to execute this file. For instance
to check constraints on test instances in the fsm project run:

./gradlew -q :mdsebook.fsm.scala:run

To do the same for the various printers meta-models run:

./gradlew -q :mdsebook.printers.scala:run

(the printers constraint driver returns more useful output, if you need to test).

Some projects have empty Constraint.scala files which can be completed with
your constraints.

---
This README file will be updated with more notes as we advance in the course.
---
